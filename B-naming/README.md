# Naming conventions

Avoid special characters whenever possible when naming files and folders. This eases working in terminals, especially Unix systems and on HPC. Try to limit to only `-`, `_` and `.` for suffices.

Plus, develop some standards by yourself or inspired by other people, and try to be consistent. This can ease parsing of the files or folder structures.

Instead of this:

```
jane Doe mouse$31 day#4 rec. on 2/3/2021 @3:23.pi-camera.csv
JD - subj #0031 {day 4} - 2021-03-02 at 3:23.microscope.xml
```

maybe do something like this: `_` to separate sections and `-` to replace white space or represent subsection

```
JD-031_2021-02-03_15-23-31_pi-camera_day004.csv
JD-031_2021-02-03_15-23-31_microscope_day004.xml
```

The `day004` can be optional. 

When doing simulations or models: 

```
vary param Vrest = -70 mV, Vthres = -20.0 mV _ simulation # 201.npy
vary param Vrest = -50 mV, Vthres = -10.3 mV _ simulation # 1020.npy
```

maybe do:

```
rest-and-thres_Vrest[-70.0mV]_Vthres[-20.0mV]_000201.npy
rest-and-thres_Vrest[-50.0mV]_Vthres[-10.3mV]_001020.npy
```

This does not follow the "limit special characters" too closely. But still makes it easy to parse.

Another way would be to have a CSV file (or any type of database like file) that represents a table of variations and name of each file, essentially:

```
sim-000001.npy
sim-000002.npy
sim-000003.npy
sim-000004.npy
```

Then the CSV file:

| sim_id | V_rest | V_thres | file_path |
| ---: | ---: | ---: | ---: |
| 1 | -70 | -20 | sim-000001.npy |
| 2 | -75 | -25 | sim-000002.npy |
| 3 | -65 | -20 | sim-000001.npy |

## FAQ

> Should there be one standard across the lab?

You can choose to agree on a single standard in a lab if you can communicate with each other and maintain some centralized way of doing so. 

However, that might not be always optimal due to the variations and complexities of the experiments and studies in the lab. 

You can try to come up with your own standard, given what you know about your project, and try to stay as consistent as possible within that project. You should also communicate with your collaborators of such project (not necessarily the entire lab) and agree upon a standard to follow through. 

As Jason suggested, keep the documentation of such standards and the different scenarios inside relevant project repositories. It can be a text file, or even a some form of metadata/schema file if you feel up to it. 

