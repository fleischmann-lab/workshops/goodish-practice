# Practice

```bash
conda env create -f environment.yaml
conda activate tutorial-LIF
# python -m ipykernel install --user --name tutorial-LIF
```

## FAQ/Comments

> Notes on `environment.yaml` and `conda` environment by multiple people

- For reproducibility purposes, also export the environment files. The one shown here is just a minified version, you should also export (e.g. after activated `conda env export  > environment-exported.yml`) or freeze the requirements (`pip freeze > requirements-frozen.txt`). Essentially you then would have:
    - one minified requirement/environment file
    - the other with a full list of all the requirements and their specific versions in the exported/frozen file
- Whenever possible when using a `conda` environment, use `conda install` instead of `pip install` as they don't work well together. 
    - If a package is not found on `anaconda` by any channels (e.g. `conda-forge`, `bioconda`), then go to `pip`
    - If a package is very outdated on `anaconda` (can happen sometimes), then go to `pip`
    - Sometimes you might want to include channels like `conda install -c conda-forge ...`
    - If a `conda install` takes a long time, either you can use `pip` to handle those packages or look into `mamba` (<https://mamba.readthedocs.io/>) as it is a faster dependency resolver than `conda`

> `conda` environments take a lot of space on my laptop

- First, list all environments using `conda env list`, this will also spell out where they are stored
- Assess which ones are not necessary anymore and how much disk space they take (e.g. in Linux you can do `du -sh <folder>`). But don't mess around with the `base` environment.
- Decide if you want to delete or archive them. You might also want to save the exported environment files for safekeeping: e.g.

    ```bash
    conda activate My-Obsolete-Env
    conda env export > my-obsolete-env-exported.yml
    conda deactivate
    ```
- If you want to archive: move the environment folder that you find in the first step into an external hard drive
- If you want to delete: `conda env remove -n My-Obsolete-Env`. Then check again with `conda env list`

Additionally, you can look into clearing cache of `pip` or `conda`, like `conda clean`. You should also look at this thread <https://stackoverflow.com/a/48706601>

> Should we create a new `conda` environment everytime? 

This depends on you. `conda` attempts to keep things separate so that your projects do not conflict with each other in their dependencies. 

For short-term tutorials or demonstrations like this, you can create then delete later. You can also look into `virtualenv`, `venv` to have the environment folders locally, which you can delete easily afterwards if need be.

For long-term projects and development, you should consider create a separate environment, and maintain the environment file frequently. 

There are however environments whose uses and purposes span across projects, like `suite2p` or `dlc`, and that is fine. You don't need to create a new `suite2p` environment for every single new project if not needed.

You can also have a couple of "dirty" playground environments for yourself that is not project-bound, like a data visualization environment that you sometimes want to do a quick and dirty dataviz.

> Do I need to install `jupyter` in every single environment?

Not necessarily.

If you use VSCode with `jupyter` then you just need to look over in the right hand side, click on it and change environments. 

Sometimes if you don't see the environment that you just literally create, try this:

```bash
conda activate My-Literally-Newly-Created-Env
python -m ipykernel install --user --name My-Literally-Newly-Created-Env --display-name My-New-Env
```

You can also choose to have a "base" `jupyter` environment as well, in which you can install extensions into. This is a bit more involved, contact Tuan if you want to do this.

Alternatively, you can also look into [`JupyterLab Desktop`](https://github.com/jupyterlab/jupyterlab-desktop)
