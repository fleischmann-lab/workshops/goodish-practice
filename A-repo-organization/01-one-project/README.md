# One project

You'd see this format quite often. This takes inspiration from `cookie-cutter` templates for data science, as well as the [lab template](https://gitlab.com/fleischmann-lab/copier-template)

## Description 

### `README.md` file

Describe your project in this file. Head to [`example_README.md`](./example_README.md) for some hints. 

### `requirements.txt`, `environment.yaml`, ... files

These describe the dependencies to reproduce your project. These can be minified version of the environment files. Additionally, you should also export environment or freeze requirements to have an additional file for full-detailed description of the project environment. Head over to [`C-practice-LIF/README.md`](../../C-practice-LIF/README.md) for more information. 

### `data` folder

This is a data folder, usually people tend to `.gitignore` the actual files in here because data binary files are usually very big. But we tend to keep the data structure. So we create the `.gitkeep` files (or just any file actually).

The subfolders inside are separated into the how much processed the data are. Go into the `data/raw` subfolder to explore an example of how to organize your raw data, which can be 

```
└── [area: optional]
    └── [animal-id: e.g. <initial>-<number>]
        └── [day (day-002) or date (2023-03-03)]
            └── [modality, instrument of recording or preprocessing ...]
                └── [step: raw or preprocessed]
```

Sometimes you can also choose to merge `area`, `animal-id` and `day` hierarchy, e.g. `PCx/KB-038/2023-03-06/` becomes `PCx_KB-038_2023-03-06/` to cut down on the depth of your folders.

### `notebooks` folder

For many projects that use `Jupyter` notebooks, this is the place to store them. Prefix their names with numbers and letters to indicate their stages and relations with each other. This helps other people to know how and which sequence to run your notebooks (especially due to sorting of file names)

For example:

```
A1-preprocess-sequencing.ipynb
A2-build-genetics-network.ipynb
A3-analyze-genetics-network.ipynb
A4-visualize-genetics-results.ipynb
B1-convert-calcium-imaging-data.ipynb
B2-build-svm-models-pcx.ipynb
B3-summarize-pcx-performance.ipynb
```

can be better than 

```
analyze-genetics-network.ipynb
build-genetics-network.ipynb
build-svm-models-pcx.ipynb
convert-calcium-imaging-data.ipynb
preprocess-sequencing.ipynb
summarize-pcx-performance.ipynb
visualize-genetics-results.ipynb
```

### `figures` folder

As the name implies, this can be a place for your figures. 

Of course, they usually should be further organization, like the "stages" of the processing or analysis.


### `docs` folder

Usually this is a place for:

- Documentations of how to use your softwares, or demos of your data/analysis
- Manuscripts, or at the official versions (like for publications)
- Reviews of the manuscripts, either unofficially from others or from publishers (remember to ask for their permissions first)
- Posters designs, files and/or `latex` 
- Sometimes can also be a place for logos, general non-data media
- Other guidelines, ...


### `src` folder

Usually short for `SOURCE`, this is the place usually used to put your scripts, functions and classes definitions that you don't want to copy and paste in many places, but use in many places. It can be a collection of helpers for the notebooks in `notebooks`. 

For example, the most common one would be:
- `utils.py`: usually you can put most **util[itie]s** functions/classes in here
- you can also put them in more contained files designated to their purpose or stage as well, like:
    - `preprocess.py` for only preprocessing purposes
    - `analysis.py` for actual analysis scripts and pipelines
    - `visualization.py` for more plotting utility functions

### `config` folder

Usually this can be a place for your to store certain program configurations, like plotting or certain analysis configs. 

For example, sometimes you have a set of 10 different parameters for your models or certain analysis. And you want certain fonts, certain font sizes, certain line widths, ... 

You can save such "meta-data" here, especially the plotting configs can then be loaded and used across many notebooks. 

But more improtantly, if your analysis has more than, say, 10 hyperparameters, it would help you and the people looking at your repo if it is in a `yaml` or `toml` file.
