# Project name

## Project description

Description: ...

Authors & Collaborators: ...

Published in: ... if already published 

## Requirements

Describe the softwares, packages, ... to reproduce your code

## Data

Include how to obtain, or download your data, which data repository you use

Sometimes you can also include the DOI of the repository 

## Demo

If you want to include instruction to run demonstration of how to view your data

## Organization 

Describe the organization of your project repository

## Reproduce or Steps

You can include how to reproduce your experiments, model building steps or analyses that you take. This can be somewhat combined with `Demo` or `Organization` if you want.

If you already publish, sometimes it's good to include how figures in the final publication are produced, e.g. which notebook produces `Figure 2a`. 

## Cite 

If this has already been published or in some archive like `arXiv` or `bioRxiv`, you can include the reference.

...
