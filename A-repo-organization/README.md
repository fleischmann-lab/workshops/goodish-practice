# Repository and project organization 

## Description

This is a collection of some ways to organize your project repository. 

Browse into each folder to see some examples and decide for yourself which one is more appropriate for you.

## Further resources

There are other examples in the wild that you can adopt as well. Keywords are: `cookiecutter template`, `copier template`, `template`. 

Or you can follow examples from published studies - look for the `Data and Code availability` or `Supplement` sections, then search for terms like `github`, `gitlab`, `zenodo`, `osf`, ...

## FAQ

> What are the `.gitkeep` and `.gitignore` files?

These are essentially files whose rules decide what files would be committed into `git` for version control. Many times `.gitignore` is sufficient. For example, you might not want to keep large raw binary data files in `git` (`git` is designed to version control text-like files more efficiently), `.ipynb_checkpoints` folders (usually redundant) or secret files (passwords, API tokens, ...)

You can browse for `.gitignore` templates online. For example, Andrea suggested <https://gitignore.io/> is a good resource (just copy and paste it in a `.gitignore` file). Sometimes the tools you use or Github/Gitlab also suggest templates to choose from. 

For difference between `.gitkeep` and `.gitignore`, Andrea suggested this answer <https://stackoverflow.com/a/7229996/4129062>

> Why have `data` folders then?

There does not seem to be a consensus on whether or how to keep the `data` folders, because there are usually large binary files. There are a few options to look into:

- You can use *data version control* tools like [`dvc`](https://dvc.org/) to synchronize it with another remote (like GDrive, ...)
- You can use symlinks to create a link to another external drives, especially because you don't have enough storage on your host machine. As Jason mentioned, this is what Oscar does. 
- You choose **not** to include the folder, but at the **top** of your notebooks, leave a code cell that decides all the relevant data file and folder paths, as Jason suggested.

When working locally, you can `.gitignore` this entire `data` folder but all your codes still reference the paths as if the files are there. When you submit for publication or publish, you can zip or tar the entire `data` folder, and put it on <https://figshare.com/>, <https://zenodo.org/> or your favorite data repository, and include instructions to download & extract the file in appropriate places. 

If you have other suggestions, feel free to open an `Issue` or `Merge request`.
