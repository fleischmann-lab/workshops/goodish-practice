# One project with multiple stages

This is an example of a repository with multiple stages, almost linearly (e.g. `experiment -> conversion -> models -> analysis`). All the folders or files with `[_span_]` means that they can either stay on the "root" folder or inside each stage folder.
