# Multiple-project in one repository

## Context 

This format can happen for different reasons, but could be because this is a publication with multiple sections that are not hierarchically/serially linked, i.e. they can share some data, codes or results but they (for example) operate almost independently in each lab and in parallel. 

Sometimes this type of organization is already at the final stage, near to publication. Or sometimes if you already know what you're doing.

## Organization

- The order of the folders can be arbitrary, or they can hint at the order of the figures in publications. 
- Each folder then can follows single-project-like organization. 
- And `docs` can be a place to aggregate and collaborate across the different sub-projects.
