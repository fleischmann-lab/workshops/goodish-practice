# "Good-ish" data and coding practices

## Description 

This is a collection of some data and coding practices that are "good-ish". 

Why *"good-ish"*? Because these are just suggestions, and depending on your own projects and situations, you'd usually have to adopt different "soft" standards. 

The goal of this collection and you designing/testing your own standards is to:

- be more organized and hopefully avoid certain pitfalls
- make it easier to reproduce your projects later
- and make it easier for others to collaborate

Go to each folder and explore, you can also clone with

```bash
git clone https://gitlab.com/fleischmann-lab/workshops/goodish-practice
```

## Further resources 

This was a workshop on Mar 06, 2023. Some of the folders have been updated with some questions raised during the workshops, to the best of Tuan's memory. 

In addition, there are many resources out there for "best practices", for example the book [*Computing for biologists*](https://computingskillsforbiologists.com/). 

Feel free to raise an `Issue` or make a `Merge request` to add more tips and tricks, or if there needs any correction to the materials, links or credits.
